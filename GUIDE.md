# Hosting custom Splatfest lobby: a guide

## Introduction

This guide aims to provide detailed information on how to act as a lobby host for a custom Splatfest.
It does not aim to provide information on how to organize the custom Splatfest as whole, only how to
host a lobby for such custom Splatfest. More importantly, this guide assumes that
[kettle](https://gitlab.com/ofiala/kettle) is already setup. Setting up kettle is highly
environment-dependent and completely beyond the scope of this guide. Unless you have enough knowledge
yourself, you should find someone with appropriate technical knowledge who will set it up and maintain
it for you.


## Software setup

Recording match results in enough details when hosting a lobby can be a huge pain in the ass, which is
why we are going to use a tool called splatnet2kettle to automate this. This tool requires a server
called kettle set up specifically for your custom Splatfest (as mentioned in Introduction).


### Setup Python environment

Go to <https://www.python.org/downloads/> and download the latest Python installer for your platform.
The website usually figures out which system you use and suggests you the correct download. If that's
the case, just click on the big yellow button right below the heading "Download the latest version for
\<your platform\>". Please note, that some operating systems (such as many Linux distributions) include
Python by default. If you are not sure, [open a terminal](#opening-a-terminal) and run `python --version`.
If it's already installed, the command will return your installed version of Python.

Once you have downloaded the installer, run it to install Python. On Windows, make sure to check
the "Add Python \<version\> to PATH" option. Select "Customize installation" and make sure
that `pip` is included in the installation, then make it install.


### Download splatnet2kettle

Go to <https://gitlab.com/ofiala/splatnet2kettle/-/releases> and grab the ZIP archive of the latest release.
Once the download finishes, unpack the archive somewhere. [Open your terminal](#opening-a-terminal).

Determine absolute path to the directory where you unpacked the archive.

**Important notes for Microsoft Windows users:**
Windows likes to hide the actual paths from their users, so please note, that the user's home directory's absolute path
on Windows is `C:\Users\<username>`. Thus, if you're a Microsoft Windows user named John and unpacked the archive into
the `Downloads` folder in your home directory, the absolute path would be `C:\Users\John\Downloads`. Also note, that
Microsoft Windows translates directory names only visually. This means that even if your system is in Czech and you see
`Stažené soubory` instead of `Downloads`, the directory's path is still `Downloads`.

Run `cd "<insert the absolute path here>"` to move to the directory where you unpacked the archive, then
`cd splatnet2kettle-v<the version you downloaded>` to move inside the unpacked directory. Now run
`pip install -r requirements.txt`. This will install all the required Python packages to run splatnet2kettle.


### Setup splatnet2kettle

Now we need to setup splatnet2kettle so that it is able to access your SplatNet 2 battle records, knows which kettle
instance to use, etc. Run `python splatnet2kettle.py`. It will ask you for the URL to your kettle instance. Enter the
URL provided by your kettle instance administrator and hit enter. Don't forget to add the protocol prefix (`http://`
or `https://`). Then, enter the API key provided by your kettle instance administrator. If the API key is okay, choose
your locale (or just hit `Enter` to use English). After that, you will be guided to obtain your session token, which is
used to access SplatNet 2. Don't worry, no data is collected by us and Nintendo hasn't banned anyone for doing this
since the launch of Splatoon 2.

Note: if you need to copy anything from the terminal, use `Ctrl` + `Shift` + `C`. **Do not use `Ctrl` + `C`**, which
for historical reasons terminates the currently running program.

Once splatnet2kettle obtains your session token, it saves all the data in the file `config.txt`. If you ever need to
re-run the setup, just delete this file. Exit splatnet2kettle by entering `0` when it prompts for number of battles
to upload. The setup is done.


## Custom Splatfest preparations

### Gather information

You will need to know the exact start and end times of the custom Splatfest. This should be determined and (ideally)
communicated by the custom Splatfest's main organizer. You will also need to know which of the teams will be alpha and
which will be bravo. This should be determined by the custom Splatfest's main organizer and implemented in kettle by
the kettle instance administrator.


### Setup communication channels

You should have reliable ways to quickly communicate with all players in your lobby, with the kettle instance
administrator in case anything goes wrong, and with other lobby hosts (if there are any). If you're using Discord, the
best way to communicate with the players in your lobby is to have a dedicated voicechat room just for your lobby.

### Setup hardware

Alter your gaming setup in such a way that you will be able to check splatnet2kettle and your communication channels
between matches without issue. For desktop-based setups, a dual monitor setup (with one connected to Switch and one to
a computer) is probably the best solution. For TV-based setups, having a laptop next to you should solve the issue.

### Give players your friend code

Players will need to have you in Friends, which is why you should give them your FC in advance so that they can add
you.

## It's showtime

### Accept friend requests

About 20 minutes before the scheduled custom Splatfest start, accept **all** pending friend requests. You should also
warn players in advance that later submitted friend requests might not be accepted.

### Add team prefix into your nickname

If your custom Splatfest's main organizer set prefixes for players of each team, add the prefix corresponding to your
team in front of your nickname. If the result is longer than 10 characters, shorten/cut off the end of your nickname.

### Launch lobby

About 15 minutes before the scheduled start, launch Splatoon 2. Create a private battle lobby open to everyone (without
any password). Set mode to Turf War (Splatfest).

If your custom Splatfest's main organizer set teams' colors, set them in Stage select -> Ink colors. If he did not, set
some appropriate ones instead so that the colors are consistent.

If your custom Splatfest's main organizer set stage schedule, limit the stages to the current rotation by disabling all
other ones by pressing `R`. If he did not, leave them random.


### Run splatnet2kettle

[Open your terminal](#opening-a-terminal). Navigate to the location where you saved it when setting up the software.
Run it in monitoring mode like this: `python splatnet2kettle.py -M 60` (checks for new battles every `60` seconds).

### Wait

Wait until you've got at least x number of players of each team (x = 2, unless the custom Splatfest's main organizer
says otherwise), or one of the teams has 4 players. Check that all players have valid prefixes. If they don't, let
them know using your communication channel. They will need to close Splatoon 2, edit their nickname, then launch it
again.

### Start battling

Once you've got enough players of both teams in your lobby, press Ready, set the teams according to players' prefixes,
then confirm. If you've got enough of players of one team (>=4) and not enough of the other, follow section "Too many
players of one team" below instead.

### Problem solving

Special cases and their solutions follow.

#### Too many players of one team

If your custom Splatfest has multiple lobbies, you can try to arrange player exchange with one of the other lobbies,
assuming they have enough players of the outnumbered team. Otherwise, do a mirror match by setting different color,
splitting the major team in half, assign first half to team alpha, second to bravo. Return colors to normal after the
mirror match. kettle will ignore this match.

#### Excluding players

If you want to exclude players from the match (such as excluding all players from the other team when doing a mirror
match), simply set them to spectate.

#### More than 2 spectators

Splatoon 2 doesn't allow more than 2 spectators. If you need to exclude more than 2 players, assign them to their
opponents' team and ask them not to do anything. Example: you want to do a 2v2 match and have another 3 alpha players
spectate. So, you set 2 alpha to play, 2 to spectate, and 1 to play as bravo, resulting in 2v3 match. You ask the extra
"bravo" player to stay at spawn and do nothing. kettle will remove the alpha player from bravo, resulting in a clean
2v2 match being recorded.

#### Match not eligible for upload

kettle may reject uploaded battle in these cases:
- one of the teams has less valid players than the configured minimum
- one of the teams was weakened (by a disconnect or otherwise) and lost

Valid players are those players, whose team corresponds to their prefix. Additionaly, it is forbidden to swap prefixes.
Based on this information, you can check the conditions under which a match took place and find, why it was rejected.
It is up to you to decide whether the time spent with this is worth it.

## Notes

### Opening a terminal

For many non-technical people, opening a terminal might be a thing they have never done before. If you are one of them,
here is how to do it on the most popular platforms:

- Windows:
	1. press `Win` + `R`, prompt shows up
	2. enter `cmd`
	3. press `Enter`
	4. done
- most Linux distributions:
	1. press `Ctrl` + `Alt` + `T`
	2. done
- MacOS:
	1. press `Cmd` + `Space`, search shows up
	2. enter `terminal`
	3. press `Enter`
	4. done
