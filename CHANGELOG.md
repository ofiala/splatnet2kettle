# Changelog

## [1.0.6] - 2021-09-30

This patch updates HTTP headers used in requests to SplatNet 2 to maintain compatibility with Nintendo's servers.


### Fixed

- fixed SplatNet 2 returning "Update required" on all requests (patch from upstream)


## [1.0.5] - 2021-09-19

This patch updates HTTP headers used in requests to SplatNet 2 to maintain compatibility with Nintendo's servers.


### Fixed

- fixed SplatNet 2 returning "Update required" on all requests (patch from upstream)


## [1.0.4] - 2021-06-29

This patch updates HTTP headers used in requests to SplatNet 2 to maintain compatibility with Nintendo's servers.


### Fixed

- fixed bug in which the script may fail to run if config doesn't exist (patch from upstream)
- fixed SplatNet 2 returning "Update required" on all requests (patch from upstream)


## [1.0.3] - 2021-02-20

This patch updates HTTP headers used in requests to SplatNet 2 to maintain compatibility with Nintendo's servers.


### Added

- added link to guide for lobby hosts to the README


### Fixed

- fixed SplatNet 2 returning "Update required" on all requests (patch from upstream)


## [1.0.2] - 2020-12-08

This patch updates HTTP headers used in requests to SplatNet 2 to maintain compatibility with Nintendo's servers.


### Fixed

- corrected inaccurate licensing info
- fixed SplatNet 2 returning "Update required" on all requests (patch from upstream)


## [1.0.1] - 2020-09-18

This patch updates HTTP headers used in requests to SplatNet 2 to maintain compatibility with Nintendo's servers.


### Fixed

- fixed SplatNet 2 returning "Update required" on all requests (patch from upstream)


## [1.0.0] - 2020-08-29

splatnet2kettle is a fork of splatnet2statink v1.5.5. This section thus describes the differences
between splatnet2kettle v1.0.0 and splatnet2statink v1.5.5.


### Added

- added -d, -S, -E flags


### Changed

- changed project name to splatnet2kettle
- switched to separate versioning (1.5.5 -> 1.0.0)
- README rewritten from scratch
- replaced stat.ink endpoints with kettle endpoints
- switched uploaded battles list parsing from JSON to MessagePack to conform with kettle
- revamped server response parsing after battle upload
- changed battle uploading to ignore all battles outside of custom Splatfest ones


### Removed

- completely removed Salmon Run
- removed image posting
- removed -t, -s, -i flags
- removed UUID
- removed dbs.py autoupdate


### Fixed

- fixed rare bug where all players except self would have Null name & SplatNet ID during upload
