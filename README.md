# splatnet2kettle: upload battles from SplatNet 2 to kettle

splatnet2kettle is a script that takes custom Splatfest battles from Nintendo's SplatNet 2, processes
them, and uploads them to kettle, API for recording custom Splatfests' results.


## Requirements

### Python environment with pip

Functional Python environment with the Python package installer `pip` is required. Installer for your
platform can be found on [Python's official website](https://www.python.org/downloads/). Please note,
that some platforms (especially most Linux distros) include Python environment by default. You can
check Python's presence by executing `python --version` in your terminal. You should also check
whether your installation includes `pip`, which can be done by executing `pip --version`.


## Installing dependencies

*Note: `cd` to this directory first.*

Running `pip install -r requirements.txt` should automatically install all dependencies.


## Running

*Note: `cd` to this directory first.*

Execute `python splatnet2kettle.py` to run the script. Available options can be displayed by adding
the `-h` flag. For basic custom Splatfest usage, running in monitoring mode (`-M` flag) is
recommended. On first run, you will be prompted to enter your kettle instance's URL, API key, and to
grant the script access to your SplatNet 2. Don't worry though, giving the script your session token
won't allow it to access any other Nintendo's services due to how Nintendo's infrastructure is
designed. All of these settings can be changed in the future by manually editing the `config.txt`
file, or completely reset by deleting it.

## How to use

See [GUIDE.md](GUIDE.md) for a comprehensive guide on hosting custom Splatfests with kettle + splatnet2kettle.
